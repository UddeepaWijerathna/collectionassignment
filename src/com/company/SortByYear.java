package com.company;

import java.util.Comparator;

public class SortByYear implements Comparator<Movie> {
    @Override
    public int compare(Movie a, Movie b) {
        return a.getYear()-b.getYear() ;
    }
}