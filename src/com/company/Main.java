package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        MovieRecord movieRecord = MovieRecord.getInstance();

        System.out.println("Sorting By Name");

        movieRecord.insertMovieWhileSortingByName(new Movie(4, "The Notebook", 2004, 2, 1));
        movieRecord.insertMovieWhileSortingByName(new Movie(5, "Titanic", 1997, 11, 1));
        movieRecord.insertMovieWhileSortingByName(new Movie(4, "DON", 2012, 2, 1));
        movieRecord.insertMovieWhileSortingByName(new Movie(5, "Harry Potter", 1999, 2, 1));
        movieRecord.insertMovieWhileSortingByName(new Movie(2, "Spider Man", 2002, 2, 1));

        movieRecord.printMovieRecord();


    }


}
