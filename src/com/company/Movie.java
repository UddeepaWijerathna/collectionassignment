package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Movie implements Comparable<Movie> {

    private double rating;
    private String name;
    private int year;
    private int month;
    private int day;

    @Override
    public int compareTo(Movie o) {
        if (this.year > o.year)
            return 1;
        else if (this.year < o.year)
            return -1;
        else if (this.month > o.month)
            return 1;
        else if (this.month < o.month)
            return -1;
        else if (this.day > o.day)
            return 1;
        else if (this.day < o.day)
            return -1;
        else
            return 0;
    }

    public Movie(double rating, String name, int year, int month, int day) {
        this.rating = rating;
        this.name = name;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public double getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "rating=" + rating +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }
}
