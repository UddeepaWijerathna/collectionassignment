package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MovieRecord {

    private static MovieRecord movieRecord;

    private List<Movie> movies;

    private MovieRecord() {
        movies = new ArrayList<Movie>();
    }

    public static MovieRecord getInstance() {
        movieRecord = movieRecord == null ? new MovieRecord() : movieRecord;
        return movieRecord;
    }

    public int movieRecordSize(){
        int size = movies.size();
        return size;
    }

    public void insertMovieWhileSortingByName(Movie movie) {

        movies.add(movie);
        Collections.sort(movies, new SortByName());

    }

    public void insertMovieWhileSortingByYear(Movie movie) {

        movies.add(movie);
        Collections.sort(movies, new SortByYear());

    }

    public void printMovieRecord(){
        Iterator itr = movies.iterator();

        while(itr.hasNext()) {
            Movie movie = (Movie) itr.next();
            System.out.println(movie.toString());
        }
        System.out.println("");
    }


}
