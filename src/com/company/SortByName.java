package com.company;

import java.util.Comparator;

public class SortByName implements Comparator<Movie> {
    @Override
    public int compare(Movie a, Movie b) {
        return a.getName().compareTo(b.getName()) ;
    }
}